#!/usr/bin/env node

// TODO: Move to config
var apiKey = '...';

// Program

var program = require('commander');
var request = require('request');
var chalk = require('chalk');

program
  .version('0.0.1')
//  .usage('[options] <keywords>')
  .option('-p, --projects', 'output projects information')
  .option('-i, --issues', 'output issues information')
  .option('-me, --whoami', 'output information about me')
  .parse(process.argv);

if(program.projects) projects(handleProjectsLoadSucceed, handleError);
else if(program.issues) issues(handleIssuesLoadSucceed, handleError);
else if(program.whoami) currentUser(handleCurrentUserLoadSucceed, handleError);
else program.help();

// Requests projects
function currentUser(succeed, failed) {
  request({
    method: 'GET',
    headers: {
      "X-Redmine-API-Key": apiKey
    },
    url:"http://pm.undev.cc/users/current.json"
  }, function (error, response, body) {

    if (!error && response.statusCode == 200) {
      var body = JSON.parse(body);
      succeed(body);
    } else if (error) {
      failed(error);
    }
  });
}

function projects(succeed, failed) {
  request({
    method: 'GET',
    headers: {
      "X-Redmine-API-Key": apiKey
    },
    url:"http://pm.undev.cc/projects.json?offset=0&limit=10"
  }, function (error, response, body) {

    if (!error && response.statusCode == 200) {
      var body = JSON.parse(body);
      succeed(body);
    } else if (error) {
      failed(error);
    }
  });
}

function issues(succeed, failed) {
  request({
    method: 'GET',
    headers: {
      "X-Redmine-API-Key": apiKey
    },
    url:"http://pm.undev.cc/issues.json?offset=0&limit=10"
  }, function (error, response, body) {

    if (!error && response.statusCode == 200) {
      var body = JSON.parse(body);
      succeed(body);
    } else if (error) {
      failed(error);
    }
  });
}

// Handlers
function handleProjectsLoadSucceed(data) {
  var projects = data.projects;

  if(projects && projects.length > 0) {
    projects.forEach(function (project) {
      console.log(project.id, project.identifier, project.name)
    });
  } else {
    console.log(chalk.red('WARN: There is no projects'));
  }

  process.exit(0);
}

function handleIssuesLoadSucceed(data) {
  var issues = data.issues;

  if(issues && issues.length > 0) {
    issues.forEach(function (item) {
      console.log(item.id, item.subject, item.status.name, item.project.name)
    });
  } else {
    console.log(chalk.red('WARN: There is no issues'));
  }

  process.exit(0);
}

function handleCurrentUserLoadSucceed(data) {
  var user = data.user;

  if(user) {
    console.log('You are %s %s', user.firstname, user.lastname);
  } else {
    console.log(chalk.red('WARN: Cannot get information about you'));
  }

  process.exit(0);
}


function handleError(error) {
  console.log(chalk.red('Error: ' + error));
  process.exit(1);
}